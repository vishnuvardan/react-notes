export const FILTER_TYPE = {
    STARRED: 'starred',
    DELETED: 'deleted',
    ALL: 'all'
}