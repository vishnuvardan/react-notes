import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Notes from './notes';
import reportWebVitals from './reportWebVitals';
import './../node_modules/@fortawesome/fontawesome-free/css/all.min.css'; 
import { Provider } from "react-redux";
import store from "./redux/reducer";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Notes />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
