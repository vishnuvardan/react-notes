import './mock.scss';

function Mock() {
    return (
        <div className="notes-wrappper">
            <div className="notes-header">
                <h1 className="notes-head">Notes</h1>
                <div className="notes-search">
                    <input type="textbox" className="generic-textbox" placeholder="Search by title"></input>
                </div>
                <div className="notes-user">
                    Welcome <span className="notes-username">Vishnuvardan Ramia Shunmugam</span>
                </div>
            </div>

            <div className="notes-body">
                <div className="notes-filter">
                    <ul>
                        <li className="active">All (5)</li>
                        <li>Starred (1)</li>
                        <li>Deleted (2)</li>
                    </ul>
                </div>
                <div className="notes-list">
                    <h2>All notes</h2>
                    <div className="notes-item">
                        <div className="notes-title">
                            <div className="notes-title-view">
                                <h3>Personal Notes</h3>
                                </div>
                            <div className="notes-action-buttons">
                                <i className="fas fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="fas fa-trash"></i>
                            </div>
                        </div>
                        <div className="notes-summary">
                            Some random personal note.
                        </div>
                    </div>
                    <div className="notes-item selected">
                        <div className="notes-title">
                            <div className="notes-title-view">
                                <h3>Personal Notes</h3>
                            </div>
                            <div className="notes-action-buttons">
                                <i className="fas fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="fas fa-trash"></i>
                            </div>
                        </div>
                        <div className="notes-summary">
                            Some random personal note.
                        </div>
                    </div>
                    <div className="notes-item">
                        <div className="notes-title">
                            <div className="notes-title-view">
                                <h3>Personal Notes</h3>
                                <div className="notes-deleted">Deleted</div>
                            </div>
                            <div className="notes-action-buttons">
                                <i className="fas fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="fas fa-trash"></i>
                            </div>
                        </div>
                        <div className="notes-summary">
                            Some random personal note.
                        </div>
                    </div>
                </div>
                <div className="notes-content-wrapper">
                    <div className="notes-content-wrapper-padding">
                        <div className="notes-content-top">
                            <div className="notes-content-title">
                                <h2>Personal Notes</h2>
                                <div className="notes-date">Jan 13, 2021 11:13:00</div>
                            </div>
                            <div className="notes-content-action-buttons">
                                    <i class="fas fa-pen"></i>
                                    <i className="fas fa-star"></i>
                                    <i className="far fa-star"></i>
                                    <i className="fas fa-trash"></i>
                            </div>
                        </div>
                        <div className="notes-content">
                            Some random personal note.
                        </div>
                    </div>
                </div>
            </div>

            <div className="notes-edit-wrapper">
                <div className="notes-edit">
                    <div className="notes-edit-top">
                        <h3>Edit Note</h3>
                        <div className="notes-edit-action-buttons">
                            <i className="fas fa-star"></i>
                            <i className="far fa-star"></i>
                            <i className="fas fa-trash"></i>
                        </div>
                    </div>
                    <div className="notes-edit-form">
                        <label>Title<span className="asterix">*</span></label>
                        <input type="textbox" className="generic-textbox"></input>
                        <label>Context<span className="asterix">*</span></label>
                        <textarea className="generic-textbox text-area"></textarea>
                        <div className="notes-edit-save-wrapper">
                            <button className="generic-outlined-button">Cancel</button>
                            <button className="generic-solid-button">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Mock;