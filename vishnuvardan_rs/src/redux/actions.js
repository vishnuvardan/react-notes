export const UPDATE_STAR = "UPDATE_STAR";
export const UPDATE_DELETE = "UPDATE_DELETE";
export const UPDATE_NOTE = "UPDATE_NOTE";

export const updateStar = (value, targetId) => {
    return {
        type: UPDATE_STAR,
        payload: {
            targetValue: value,
            targetId: targetId
        }
    }
};

export const updateDelete = (value, targetId) => {
    return {
        type: UPDATE_DELETE,
        payload: {
            targetValue: value,
            targetId: targetId
        }
    }
};

export const updateNote = (value) => {
    return {
        type: UPDATE_NOTE,
        payload: {
            targetValue: value
        }
    }
};