import { createStore } from 'redux';
import { UPDATE_STAR, UPDATE_DELETE, UPDATE_NOTE } from './actions';

const initialState = {
    notes: [
        {
            "id": 1,
            "createdAt": "January 13, 2021 11:13:00",
            "title": "Personal Notes",
            "context": "Some random personal note. This is not so important but still needed.",
            "starred": false,
            "deleted": false
            },
            {
            "id": 2,
            "createdAt": "January 27, 2021 09:53:00",
            "title": "Shopping Notes",
            "context": "This is where I store all my shopping related notes like shop contact details and items to purchase.",
            "starred": false,
            "deleted": false
            },
            {
            "id": 3,
            "createdAt": "February 04, 2021 16:00:00",
            "title": "Medical Notes",
            "context": "This is a very important note for all the medications that my family uses. It consists of medicine names, therapists contact details and information related to hospitals.",
            "starred": true,
            "deleted": false
            },
            {
            "id": 4,
            "createdAt": "March 14, 2021 10:10:00",
            "title": "Friends",
            "context": "This note consists of all my friends & their family details like birthdays and anniversaries.",
            "starred": false,
            "deleted": false
            },
            {
            "id": 5,
            "createdAt": "April 02, 2021 19:35:00",
            "title": "Other Notes",
            "context": "I use this note to store all other information that are not needed temporarily.",
            "starred": false,
            "deleted": true
            }
    ]
};

const notesReducer = (state = initialState, action) => {
    switch(action.type) {
        case UPDATE_STAR:
            state.notes.forEach((value) => {
                if (value.id === action.payload.targetId) {
                    value.starred = action.payload.targetValue;
                }
            });
            return state;
        case UPDATE_DELETE:
            state.notes.forEach((value) => {
                if (value.id === action.payload.targetId) {
                    value.deleted = action.payload.targetValue;
                }
            });
            return state;
        case UPDATE_NOTE:
            state.notes.forEach((value) => {
                if (value.id === action.payload.targetValue.id) {
                    value.title = action.payload.targetValue.title;
                    value.context = action.payload.targetValue.context;
                    value.starred = action.payload.targetValue.starred;
                    value.deleted = action.payload.targetValue.deleted;
                }
            });
            return state;
        default:
            return state;
    }
}

export default createStore(notesReducer);