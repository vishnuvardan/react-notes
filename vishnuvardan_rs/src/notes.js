import './notes.scss';
import React, { Component } from "react";
import { connect } from "react-redux";
import { FILTER_TYPE } from "./constants";
import NotesFilter from "./components/notes-filter/notes-filter";
import NotesSearch from "./components/notes-search/notes-search";
import NotesEdit from "./components/notes-edit/notes-edit";
import NotesDetail from "./components/notes-detail/notes-detail";
import NotesItem from "./components/notes-item/notes-item";

class Notes extends Component {
    state = {
        editMode: false,
        filterMode: FILTER_TYPE.ALL,
        searchText: '',
        notes: this.props.notes,
        activeNote: this.props.notes.length > 0 ? this.props.notes[0] : false,
        formNote: {}
    };

    // This method sets default active note.
    // If user has already has selected state, it maintains this.
    updateActivatedNote = () => {
        let activeNote = false;
        if (this.state.notes.length !== 0) {
            const isAlreadyAvailable = this.state.notes.some((value) => (value.id === this.state.activeNote.id));
            if (isAlreadyAvailable)  {
                activeNote = this.state.activeNote;
            } else {
                activeNote = this.state.notes[0];
            }
        }
        this.setState({
            activeNote: activeNote
        });
    }

    // This method takes necessary filter parameters and then filters.
    handleFilter = () => {
        const filteredNotes = this.props.notes
            .filter((note) => {
                if (this.state.filterMode === FILTER_TYPE.STARRED) {
                    return note.starred;
                }
                else if (this.state.filterMode === FILTER_TYPE.DELETED) {
                    return note.deleted;
                }
                else {
                    return true;
                }
            })
            .filter((note) => note.title.toLowerCase().includes(this.state.searchText));
        this.setState({
            notes: filteredNotes
        }, this.updateActivatedNote);
    }

    // This method updates filter state variables.
    updateFilter = (value) => {
        this.setState(value, this.handleFilter);
    }

    // This method open edit form by setting the active note values.
    editNote = () => {
        if (!this.state.activeNote.deleted) {
            const formNote = Object.assign({}, this.state.activeNote);
            this.setState({
                editMode: true,
                formNote: formNote
            });
        }
    }

    // This method closes note form.
    closeForm = () => {
        this.setState({
            editMode: false
        });
    }

    // This method activates the selected form.
    activateNote = (note) => {
        this.setState({
            activeNote: note
        });
    }

    render () {
        return (
            <div className="notes-wrappper">
                <div className="notes-header">
                    <h1 className="notes-head">Notes</h1>
                    <div className="notes-search">
                        {
                            (!this.state.editMode) ? 
                                <NotesSearch
                                    updateFilter={this.updateFilter}
                                    notes={this.props.notes}></NotesSearch>
                                    : ''
                        }
                        
                    </div>
                    <div className="notes-user">
                        Welcome <span className="notes-username">Vishnuvardan Ramia Shunmugam</span>
                    </div>
                </div>
                {
                    (!this.state.editMode) ? (
                    <div className="notes-body">
                        <NotesFilter
                            updateFilter={this.updateFilter}
                            filterMode={this.state.filterMode}
                            notes={this.props.notes}></NotesFilter>
                        <div className="notes-list">
                            <h2>All notes</h2>
                            {(this.state.notes && this.state.notes.length) ?
                                this.state.notes.map((note) => {
                                    return (
                                        <div
                                            key={"note" + note.id}
                                            onClick={(e) => {this.activateNote(note);}}>
                                            <NotesItem
                                                handleFilter={this.handleFilter}
                                                activeNote={this.state.activeNote}
                                                note={note}></NotesItem>
                                        </div>
                                    );
                                })
                                :
                                <div className="notes-no-results">No notes found</div>
                            }
                        </div>
                        <NotesDetail
                            handleFilter={this.handleFilter}
                            editNote={this.editNote}
                            activeNote={this.state.activeNote}></NotesDetail>
                    </div>)
                    : <NotesEdit
                        closeForm={this.closeForm}
                        formNote={this.state.formNote}></NotesEdit>
                    }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        notes: state.notes
    }
};

export default connect(mapStateToProps)(Notes);