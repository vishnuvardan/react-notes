import React, { Component } from "react";
import { connect } from "react-redux";
import { updateNote } from "./../../redux/actions";
import "./notes-edit.scss";

class NotesEdit extends Component {
    state = {
        disableSave: false,
        formNote: this.props.formNote
    }

    // This method takes care of form validation
    validateForm = () => {
        this.setState({
            disableSave: true
        });
        if (this.state.formNote.title.length > 5 && this.state.formNote.context.length > 5) {
            this.setState({
                disableSave: false
            });
        }
    }

    // This method updates form values to the respective states.
    handleFormChange = (targetValue, targetKey) => {
        const formNote = this.state.formNote;
        formNote[targetKey] = targetValue;
        this.setState({
            formNote: formNote
        });
        this.validateForm();
    }

    // This method initiates form update to store and then closes the form.
    saveNote = (note) => {
        if (!this.state.disableSave) {
            this.props.updateNote(note);
            this.props.closeForm();
        }
    }

    render() {
      return (
            <div className="notes-edit-wrapper">
                <div className="notes-edit">
                    <div className="notes-edit-top">
                        <h3>Edit Note</h3>
                        <div className="notes-edit-action-buttons">  
                            {(this.state.formNote.starred) ? (<i className={"fas fa-star " + (this.state.formNote.deleted ? 'disabled' : '')} onClick={() => {this.handleFormChange(false, 'starred');}}></i>):<i className={"far fa-star " + (this.state.formNote.deleted ? 'disabled' : '')} onClick={() => {this.handleFormChange(true, 'starred');}}></i>}
                            {(this.state.formNote.deleted) ? (<i className="fas fa-trash-restore" onClick={() => {this.handleFormChange(false, 'deleted');}}></i>):<i className="fas fa-trash" onClick={() => {this.handleFormChange(true, 'deleted');}}></i>}
                        </div>
                    </div>
                    <div className="notes-edit-form">
                        <label htmlFor="noteTitle">Title<span className="asterix">*</span></label>
                        <input id="noteTitle" minLength="5" required type="textbox" className="generic-textbox validation-fill-box" defaultValue={this.state.formNote.title} onChange={ (e) => this.handleFormChange(e.target.value, 'title')}></input>
                        <label htmlFor="noteContext">Context<span className="asterix">*</span></label>
                        <textarea id="noteContext" minLength="5" required type="textbox" className="generic-textbox text-area validation-fill-box"  defaultValue={this.state.formNote.context} onChange={ (e) => this.handleFormChange(e.target.value, 'context')}></textarea>
                        <div className="notes-edit-save-wrapper">
                            <button className="generic-outlined-button" onClick={this.props.closeForm}>Cancel</button>
                            <button className="generic-solid-button" disabled={this.state.disableSave} onClick={ () => {this.saveNote(this.state.formNote)}}>Save</button>
                        </div>
                    </div>
                </div>
            </div>
      )
    }
}

export default connect(null, {updateNote})(NotesEdit);