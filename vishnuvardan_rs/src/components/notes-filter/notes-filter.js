import React, { Component } from "react";
import { FILTER_TYPE } from "./../../constants";
import './notes-filter.scss';

export default class NotesFilter extends Component {
    updateFilter = (value) => {
        this.props.updateFilter({
            'filterMode': value
        });
    }

    render() {
      return (
        <div className="notes-filter">
            <ul>
                <li className={(this.props.filterMode === FILTER_TYPE.ALL ? 'active' : '')}
                    onClick={ () => { this.updateFilter(FILTER_TYPE.ALL) }}>All ({this.props.notes.length})</li>
                <li className={(this.props.filterMode === FILTER_TYPE.STARRED ? 'active' : '')}
                    onClick={ () => { this.updateFilter(FILTER_TYPE.STARRED) }}>Starred ({this.props.notes.filter((value)=>(value.starred)).length})</li>
                <li className={(this.props.filterMode === FILTER_TYPE.DELETED ? 'active' : '')}
                    onClick={ () => { this.updateFilter(FILTER_TYPE.DELETED) }}>Deleted ({this.props.notes.filter((value)=>(value.deleted)).length})</li>
            </ul>
        </div>
      )
    }
}