import React, { Component } from "react";
import { connect } from "react-redux";
import { updateStar } from "./../../redux/actions";

class NotesStar extends Component {
    updateStar = (value, id) => {
        if (!this.props.deleted) {
            this.props.updateStar(value, id);
            this.props.handleFilter();
        }
    }

    render() {
      return (
        (this.props.starred)
            ? 
                <i className={"fas fa-star " + (this.props.deleted ? 'disabled' : '')}
                    onClick={() => {this.updateStar(false, this.props.noteId);}}></i>
            :
                <i className={"far fa-star " + (this.props.deleted ? 'disabled' : '')}
                    onClick={() => {this.updateStar(true, this.props.noteId);}}></i>
        )
    }
}

export default connect(null, {updateStar})(NotesStar);