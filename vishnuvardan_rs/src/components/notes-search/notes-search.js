import React, { Component } from "react";

export default class NotesSearch extends Component {
  updateFilter = (value) => {
      this.props.updateFilter({
          'searchText': value
      });
  }

  render() {
    return (
      <input type="textbox" required
        onBlur={(e) => this.updateFilter(e.target.value)}
        className="generic-textbox fill-box"
        placeholder="Search by title"></input>
    )
  }
}