import React, { Component } from "react";
import { connect } from "react-redux";
import { updateDelete } from "./../../redux/actions";

class NotesDelete extends Component {
    updateDelete = (value, id) => {
        this.props.updateDelete(value, id);
        this.props.handleFilter();
    }

    render() {
      return (
        (this.props.deleted) 
            ? 
                <i className="fas fa-trash-restore"
                    onClick={() => {this.updateDelete(false, this.props.noteId); }}></i>
            :
                <i className="fas fa-trash"
                    onClick={() => {this.updateDelete(true, this.props.noteId);}}></i>)
    }
}

export default connect(null, {updateDelete})(NotesDelete);