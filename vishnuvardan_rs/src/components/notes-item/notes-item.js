import React, { Component } from "react";
import NotesStar from "./../notes-star/notes-star";
import NotesDelete from "./../notes-delete/notes-delete";
import "./note-item.scss";

export default class NotesDetail extends Component {
    render() {
      return (
        <div className={"notes-item " + (this.props.note.id === this.props.activeNote.id ? 'selected' : '')}>
            <div className="notes-title">
                <div className="notes-title-view">
                    <h3>{this.props.note.title}</h3>
                    {(this.props.note.deleted) ? (<div className="notes-deleted">Deleted</div>):''}
                </div>
                <div className="notes-action-buttons">
                    <NotesStar
                        handleFilter={this.props.handleFilter}
                        noteId={this.props.note.id}
                        deleted={this.props.note.deleted}
                        starred={this.props.note.starred}></NotesStar>
                    <NotesDelete
                        handleFilter={this.props.handleFilter}
                        noteId={this.props.note.id}
                        deleted={this.props.note.deleted}></NotesDelete>
                </div>
            </div>
            <div className="notes-summary">
                {this.props.note.context}
            </div>
        </div>
        )
    }
}