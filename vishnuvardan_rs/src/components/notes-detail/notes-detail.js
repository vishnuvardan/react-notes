import React, { Component } from "react";
import NotesStar from "./../notes-star/notes-star";
import NotesDelete from "./../notes-delete/notes-delete";
import "./notes-detail.scss";

export default class NotesDetail extends Component {
    render() {
      return (
        <div className="notes-content-wrapper">
            {
                (this.props.activeNote) ? 
                    <div className="notes-content-wrapper-padding">
                        <div className="notes-content-top">
                            <div className="notes-content-title">
                                <h2>{this.props.activeNote.title}</h2>
                                <div className="notes-date">{this.props.activeNote.createdAt}</div>
                            </div>
                            <div className="notes-content-action-buttons">
                                <i className={"fas fa-pen " + (this.props.activeNote.deleted ? 'disabled' : '')}
                                    onClick={this.props.editNote}></i>
                                <NotesStar
                                    handleFilter={this.props.handleFilter}
                                    noteId={this.props.activeNote.id}
                                    deleted={this.props.activeNote.deleted}
                                    starred={this.props.activeNote.starred}></NotesStar>
                                <NotesDelete
                                    handleFilter={this.props.handleFilter}
                                    noteId={this.props.activeNote.id}
                                    deleted={this.props.activeNote.deleted}></NotesDelete>
                            </div>
                        </div>
                        <div className="notes-content">
                            {this.props.activeNote.context}
                        </div>
                    </div>
                :
                    <div className="notes-content-wrapper-padding">
                        <b>No details to display</b>
                    </div>
            }
        </div>
        )
    }
}